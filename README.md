# README

This is a test of using the ransack search gem with ROR. I am creating a query with my Jeff Sackman ATP data.
Sadly, due to heroku pgsql limitations, I could only upload 6447 rows so the data is from 2000 to Feb 2016,
and only contains matches in which one of the players is in the top 5.
Also, Jeff has not updated since Feb 2016, so current data is not there.

I have used ROR 4.2.7 (the latest) and ransack 1.5.1.

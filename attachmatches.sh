#!/usr/bin/env bash 
#===============================================================================
#
#          FILE: attachdb.sh
# 
#         USAGE: ./attachdb.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 07/13/2016 19:01
#      REVISION:  2016-07-13 19:22
#===============================================================================

SQLITE=$(brew --prefix sqlite)/bin/sqlite3
MYDATABASE=db/development.sqlite3
MYTABLE=matches
$SQLITE $MYDATABASE <<!
ATTACH DATABASE "tennis.db" as orig;
INSERT INTO matches select rowid,*,'2016','2016' from orig.matches ;
select count(*) from matches;
select count(*) from orig.matches;
!

class Deletecolumnsdevelopment < ActiveRecord::Migration
  def change

    if Rails.env.development?
    remove_column :matches,  :draw_size
    remove_column :matches,  :winner_seed
    remove_column :matches,  :winner_entry
    remove_column :matches,  :winner_hand
    remove_column :matches,  :winner_ht
    remove_column :matches,  :winner_ioc
    remove_column :matches,  :winner_age
    remove_column :matches,  :winner_rank_points
    remove_column :matches,  :loser_seed
    remove_column :matches,  :loser_entry
    remove_column :matches,  :loser_hand
    remove_column :matches,  :loser_ht
    remove_column :matches,  :loser_ioc
    remove_column :matches,  :loser_age
    remove_column :matches,  :loser_rank_points
    remove_column :matches,  :best_of
    remove_column :matches,  :minutes
    remove_column :matches,  :w_ace
    remove_column :matches,  :w_df
    remove_column :matches,  :w_svpt
    remove_column :matches,  :w_1stIn
    remove_column :matches,  :w_1stWon
    remove_column :matches,  :w_2ndWon
    remove_column :matches,  :w_SvGms
    remove_column :matches,  :w_bpSaved
    remove_column :matches,  :w_bpFaced
    remove_column :matches,  :l_ace
    remove_column :matches,  :l_df
    remove_column :matches,  :l_svpt
    remove_column :matches,  :l_1stIn
    remove_column :matches,  :l_1stWon
    remove_column :matches,  :l_2ndWon
    remove_column :matches,  :l_SvGms
    remove_column :matches,  :l_bpSaved
    remove_column :matches,  :l_bpFaced
    end
  end
end

class ChangeIdFormatInMatches < ActiveRecord::Migration
  def change
    remove_column :matches, :winner_id
    add_column :matches, :winner_id, :integer
    remove_column :matches, :loser_id
    add_column :matches, :loser_id, :integer
        #change_column :matches, :winner_id, :integer USING CAST(winner_id as INTEGER)
        #change_column :matches, :loser_id, :integer USING CAST(loser_id as INTEGER)
  end
end

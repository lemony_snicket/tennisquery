class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.string :tourney_id
      t.string :tourney_name
      t.string :surface
      t.string :draw_size
      t.string :tourney_level
      t.string :tourney_date
      t.string :match_num
      t.string :winner_id
      t.string :winner_seed
      t.string :winner_entry
      t.string :winner_name
      t.string :winner_hand
      t.string :winner_ht
      t.string :winner_ioc
      t.string :winner_age
      t.string :winner_rank
      t.string :winner_rank_points
      t.string :loser_id
      t.string :loser_seed
      t.string :loser_entry
      t.string :loser_name
      t.string :loser_hand
      t.string :loser_ht
      t.string :loser_ioc
      t.string :loser_age
      t.string :loser_rank
      t.string :loser_rank_points
      t.string :score
      t.string :best_of
      t.string :round
      t.string :minutes
      t.string :w_ace
      t.string :w_df
      t.string :w_svpt
      t.string :w_1stIn
      t.string :w_1stWon
      t.string :w_2ndWon
      t.string :w_SvGms
      t.string :w_bpSaved
      t.string :w_bpFaced
      t.string :l_ace
      t.string :l_df
      t.string :l_svpt
      t.string :l_1stIn
      t.string :l_1stWon
      t.string :l_2ndWon
      t.string :l_SvGms
      t.string :l_bpSaved
      t.string :l_bpFaced
      t.timestamps null: false
    end
  end
end

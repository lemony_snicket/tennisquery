class Droptimestamps < ActiveRecord::Migration
  def change

    remove_column :matches, :updated_at
    remove_column :matches, :created_at
    remove_column :players, :updated_at
    remove_column :players, :created_at
  end
end

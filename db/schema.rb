# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160714141709) do

  create_table "matches", force: :cascade do |t|
    t.string  "tourney_id"
    t.string  "tourney_name"
    t.string  "surface"
    t.string  "tourney_level"
    t.string  "tourney_date"
    t.string  "match_num"
    t.integer "winner_id"
    t.string  "winner_name"
    t.string  "winner_rank"
    t.integer "loser_id"
    t.string  "loser_name"
    t.string  "loser_rank"
    t.string  "score"
    t.string  "round"
  end

  create_table "players", force: :cascade do |t|
    t.string "firstName"
    t.string "lastName"
    t.string "hand"
    t.string "birth"
    t.string "country"
  end

end

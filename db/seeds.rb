# encoding: utf-8
# https://gist.github.com/seyhunak/7843549
connection = ActiveRecord::Base.connection
connection.tables.each do |table|
  if true
    if Rails.env.production?
      connection.execute("TRUNCATE #{table}") unless table == "schema_migrations"
    else
      connection.execute("delete from #{table}") unless table == "schema_migrations"
    end
  end
end

files=["db/players.sql","db/matches.sql"]
if Rails.env.production?
  files=["db/m.sql"]
end

#['db/players.sql', 'db/matches.sql'].each do |file|
files.each do |file|
  #sql = File.read(file)
  #statements = sql.split(/;$/)
  #statements.pop

  #statements = File.readlines(file, 'rb:us-ascii')
  #statements = File.readlines(file)
  #puts "Statements: " + statements.count.to_s
  begin
  ActiveRecord::Base.transaction do
    IO.foreach(file) do |statement|
      #statements.each do |statement|
      connection.execute(statement)
    end
  end
  rescue => ex
    puts ex.to_s
    puts (ex.backtrace.join("\n")) if ex
  end
  puts "imported file #{file}"
end
n = Player.count
puts "#{n} Players inserted"
n = Match.count
puts "#{n} matches inserted"

#!/usr/bin/env bash 
#===============================================================================
#
#          FILE: attachdb.sh
# 
#         USAGE: ./attachdb.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 07/13/2016 19:01
#      REVISION:  2016-07-13 19:09
#===============================================================================

SQLITE=$(brew --prefix sqlite)/bin/sqlite3
MYDATABASE=db/development.sqlite3
MYTABLE=players
$SQLITE $MYDATABASE <<!
attach database "tennis.db" as orig;
INSERT INTO players (id, firstName, lastName, hand, birth, country, created_at, updated_at) SELECT id, firstName, lastName, hand, birth, country , '20160101', '20160101' from orig.player;
select count(*) from players;
    select count(*) from orig.player;
!

module MatchesHelper
  def matches_model_fields
    # which fields to display and sort by
    [:tourney_id, :tourney_name, :tourney_date, :winner_name, :loser_name, :score, :surface, :round]
  end

  def matches_results_limit
    # max number of search results to display
    10
  end

  def matches_display_query_sql(users)
    "SQL: #{users.to_sql}"
  end

  def matches_display_results_header(count)
    if count > matches_results_limit
      "Your first #{matches_results_limit} results out of #{count} total"
    else
      "Your #{pluralize(count, 'result')}"
    end
  end

  def action
    action_name == 'advanced_search' ? :post : :get
  end

  def matches_display_sort_column_headers(search)
    i = 0
    matches_model_fields.each_with_object('') do |field, string|
      if i == 0
        string << content_tag(:th, '#')
      end
      i+= 1
      string << content_tag(:th, sort_link(search, field, {}, method: action))
    end
  end

  def matches_display_search_results(objects)
    offset = objects.offset
    i = 1
    #objects.limit(results_limit).each_with_object('') do |object, string|
    objects.each_with_object('') do |object, string|
      string << content_tag(:tr, matches_display_search_results_row(object, offset + i))
      i += 1
    end
  end

  def matches_display_search_results_row(object, offset)
    flag = true
    matches_model_fields.each_with_object('') do |field, string|
      if flag
        string << content_tag(:td, offset)
        flag = false
      end
      string << content_tag(:td, object.send(field))
    end
    .html_safe
  end

end

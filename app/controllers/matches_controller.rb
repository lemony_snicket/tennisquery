class MatchesController < ApplicationController
  respond_to :html

  def index
    @search = Match.ransack(params[:q])
    #@matches  = params[:distinct].to_i.zero? ?
      #@search.result :
      #@search.result(distinct: true)

    @matches = @search.result.paginate(page: params[:page], per_page: params[:per_page])
    #@total_entries = @matches.size
    # will_paginate requires total_entries for total results, size will only give batch/page size
    @total_entries = @matches.total_entries
    respond_with @matches
  end

  # for some reason advanced search does not save the params q upon pagination,
  # so i have to save it to session. Which means that a CLEAR may be requried.
  def advanced_search

    # if there is a no value then take it to session
    if !params[:q]
      params[:q] = session[:query]
    end
    @search = Match.ransack(params[:q])
    @search.build_grouping unless @search.groupings.any?
    @matches = @search.result.paginate(page: params[:page], per_page: params[:per_page])
    @total_entries = @matches.total_entries
    if params[:q]
      session[:query] = params[:q]
    end
    #puts "saving @paramsq to session"
    #pp session[:query]

    #@matches  = params[:distinct].to_i.zero? ?
      #@search.result :
      #@search.result(distinct: true)

    respond_with @matches
  end
end
